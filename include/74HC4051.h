#ifndef _74HC4051_H
#define _74HC4051_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Total number of output channels of multiplexer.
 * You can use this define for size of arrays, related to output pins.
 */
#define x74HC4051_CHANNEL_NUM  8

/**
 * @breif Number of input pins of multiplexer. You can use this define for
 * size of arrays related to input pins.
 */
#define x74HC4051_INPUT_NUM    3

/**
 * @brief Multiplexer configuration structure. It must be set by user before
 * using library functions.
 */
typedef struct {
  /**
   * @brief User callback, which must to set given GPIO pin to the given state.
   * @param id Requested multiplexer input pin number, from 0 to 3 according
   * to multiplexer datasheet.
   * @param set Logical level to be set on pin: when true, set pin to high,
   * when false, set pin to low.
   */
  void(*set_in_f)(uint8_t id, bool set);
  /**
   * @brief user callback, which must to set GPIO pin connected to the E(6)
   * multiplexer pin.
   * @param set Logical level to be set: if true, set pin to high level,
   * if false, set pin to low.
   */
  void(*set_enable_f)(bool set);
} x74HC4051_config_t;

/**
 * @brief Enable selected output of the multiplexer. This function should be
 * called explicitly only if multiplexer was disabled by user by
 * \ref 74HC4051_disable function.
 * @param config Multiplexer configuration to be used.
 */
void x74HC4051_enable(const x74HC4051_config_t config);

/**
 * @brief Disable multiplexer outputs.
 * @param config Multiplexer configuration to be used.
 */
void x74HC4051_disable(const x74HC4051_config_t config);

/**
 * @brief Select specified multiplexer output.
 * @param config Mltiplexer configuration to be used.
 * @param out Output channel to be selected. The channel numeration starts
 * from 0 to 7, according multiplexer datasheet (0 is Y0, 1 is Y1 etc.)
 */
void x74HC4051_select(const x74HC4051_config_t config, uint8_t out);

#ifdef __cplusplus
}
#endif

#endif /* _74HC4051_H */
