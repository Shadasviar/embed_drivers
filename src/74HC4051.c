#include <74HC4051.h>

void x74HC4051_enable(const x74HC4051_config_t config) {
  config.set_enable_f(false);
}

void x74HC4051_disable(const x74HC4051_config_t config) {
  config.set_enable_f(true);
}

void x74HC4051_select(const x74HC4051_config_t config, uint8_t out) {
  if (out >= x74HC4051_CHANNEL_NUM) return;
  config.set_enable_f(true);
  for (uint8_t i = 0; i < x74HC4051_INPUT_NUM; ++i) {
    config.set_in_f(i, out & (1 << i));
  }
  config.set_enable_f(false);
}
